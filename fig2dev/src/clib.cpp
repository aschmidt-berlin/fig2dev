#include <stdlib.h>
#include <string.h>

#include <QFileInfo>

extern "C" {

#include "clib.h"

void stcgfp(char *directoryName, char *fullpath)
{
	int i;

	if(directoryName == NULL)
		return;

	if(fullpath == NULL || strlen(fullpath) == 0)
	{
		directoryName[0] = 0x00;
		return;
	}

	// find the last '\'
	for(i=strlen(fullpath)-1; i >= 0; i--)
	{
		if(fullpath[i] == '\\')
		{
			break;
		}
	}

	if(i == -1)
	{
		directoryName[0] = 0x00;
		return;
	}

	strncpy(directoryName, fullpath, i);
	directoryName[i] = 0x00;
}

void stcgfn(char *fileName, char *fullpath)
{
	int i, j;

	if(fileName == NULL)
		return;

	if(fullpath == NULL || strlen(fullpath) == 0)
	{
		fileName[0] = 0x00;
		return;
	}

	// find the last '\'
	for(i=strlen(fullpath)-1; i >= 0; i--)
	{
		if(fullpath[i] == '\\')
		{
			break;
		}
	}

	// no \ found => take full path as filename
	if(i == -1)
	{
		strcpy(fileName, fullpath);
		return;
	}

	i++;

	for(j=0; i < (signed int) strlen(fullpath); i++, j++)
	{
		fileName[j] = fullpath[i];
	}

	fileName[j] = 0x00;
}

void stcgfe(char *ext, char *fullpath)
{
	/*
	int i, j;

	if(ext == NULL)
		return;

	if(fullpath == NULL || strlen(fullpath) == 0)
	{
		ext[0] = 0x00;
		return;
	}

	// find the last '.'
	for(i=strlen(fullpath)-1; i >= 0; i--)
	{
		if(fullpath[i] == '.')
		{
			break;
		}
	}

	// not found
	if(i == -1)
	{
		ext[0] = 0x00;
		return;
	}

	i++;

	for(j=0; i < (signed int) strlen(fullpath); i++, j++)
	{
		ext[j] = fullpath[i];
	}

	ext[j] = 0x00;
*/
	QFileInfo finfo(fullpath);
	strcpy(ext, finfo.suffix().toLatin1().data());
}

// returns full path without extension
void stcgf(char *path, char *fullpath)
{
	int i;

	if(path == NULL)
		return;

	if(fullpath == NULL || strlen(fullpath) == 0)
	{
		path[0] = 0x00;
		return;
	}

	// find the last '.'
	for(i=strlen(fullpath)-1; i >= 0; i--)
	{
		if(fullpath[i] == '.')
		{
			break;
		}
	}

	if(i == -1)
	{
		path[0] = 0x00;
		return;
	}

	strncpy(path, fullpath, i);
	path[i] = 0x00;
}

void strmfp(char *fullpath, char *path, char *fileName)
{
	unsigned int i, j;
	int pathlen = strlen(path);

	if(fullpath == NULL || path == NULL || fileName == NULL)
	{
		fullpath[0] = 0x00;
		return;
	}

	if(pathlen > 0)
	{
		if(path[pathlen-1] == '\\')
			strncpy(fullpath, path, pathlen-1);
		else
			strncpy(fullpath, path, pathlen);
	}

	for(i=0, j=pathlen; i < strlen(fileName); i++, j++)
	{
		fullpath[j] = fileName[i];
	}

	fullpath[j] = 0x00;
}

}
