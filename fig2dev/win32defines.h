#pragma once

#if defined(WIN32)
#define strcasecmp _stricmp
#define strncasecmp _strnicmp 
#define strdup _strdup
#define getpid _getpid
#define pclose _pclose
#define popen _popen
#define sleep(x) winSleep(x)
#define gethostname winGethostname
#define strdup _strdup
#define unlink _unlink
#define fileno _fileno
#define read _read
#define write _write
#define fprintf _fprintf_p

#ifndef strndup
#define ssize_t int
#endif

#endif
