#pragma once

typedef int BOOL;

#ifdef WIN32

#ifdef WIN32
	#define GS_EXECXX "gswinxxc.exe"
	#define GS_EXEC32 "gswin32c.exe"
	#define GS_EXEC64 "gswin64c.exe"
#endif

struct passwd{
	char pw_name[256];
	char pw_gecos[256];
};

struct passwd *getpwuid();
void winSleep(unsigned long dwMilliseconds);
int winGethostname(char *name, unsigned long namelen);
void getGhostScriptPath(void);

extern char ghostScriptPath[1024];
extern char ghostScriptCommand[1024];

int WIFEXITED(int status);
int WEXITSTATUS(int status);

#endif

#ifndef strndup
char* strndup(const char* s, size_t n);
#endif
