//#include <afx.h>

// I have no idea what this "dev" is and where it is defined, but it conflicts with definitions in qdatastream.h
#if defined(dev)
#undef dev
#endif

#include <stdlib.h>
#include <string.h>

#include <QDir>
#include <QFileInfo>
#include <QString>
#include <QStringList>
#include <QProcess>
#include <QSettings>

#include "clib.h"

#ifdef WIN32
#include <Windows.h>
#endif

QStringList getGroupFromRegistry(QString path, QSettings::Format format)
{
	QSettings settings(path, format);
	QStringList groups = settings.childGroups();
	return groups;
}

extern "C" {

#include "win32supp.h"

BOOL executeCommand(char *command)
{
	QProcess process;
	bool success = false;
	QString message;
	QString completeCommand = command;

	process.startCommand(completeCommand);

	success = process.waitForStarted();

	if(!success)
	{
		int error = process.error();
//		qDebug("doExport: error=%d\n", error);
		message = QString("%1 failed with error code %2").arg(completeCommand).arg(error);
	}

	if(success)
	{
		success = process.waitForFinished();
	}

	return success;
}

#ifdef WIN32
static struct passwd who;

struct passwd *getpwuid()
{
	DWORD userIDbufferSize = 256;
	who.pw_name[0] = 0;
	who.pw_gecos[0] = 0;
	
	wchar_t wname[256];

	if(!GetUserName(wname, &userIDbufferSize))
	{
		who.pw_name[0] = 0;
		return NULL;
	}

	QString sname = QString::fromWCharArray(wname);
	strcpy(who.pw_name, sname.toLatin1().data());

	return(&who);
}

void winSleep(unsigned long seconds)
{
	Sleep(1000 * seconds);
}

int winGethostname(char *name, unsigned long namelen)
{
	wchar_t wname[256];
	BOOL result = GetComputerName(wname, &namelen);
	QString sname = QString::fromWCharArray(wname);
	strcpy(name, sname.toLatin1().data());
	return(result? 0 : -1);
/*
	int err = gethostname(name, namelen);

	if(err == SOCKET_ERROR)
		printf("gethostname %d\n", WSAGetLastError());

	return(err == 0? 0 : -1);
*/}

char ghostScriptPath[1024] = "";
char ghostScriptCommand[1024] = "";

#ifdef WIN32
#define ORGANIZATION "AndreasSchmidt"
#elif __APPLE__
#define ORGANIZATION "andreas-schmidt"
#else
#define ORGANIZATION "WinFIG"
#endif

#define PREFS_OTHER_USE_DEFAULT_GS_LOCATION "use default gs location"
#define PREFS_OTHER_GS_LOCATION "ghostscript location"

void getGhostScriptPath(void)
{
	// check the user overrides the GhostScript path in the WinFIG preferences dialog
	QSettings winfigSettings(ORGANIZATION, "WinFIG");

	bool useDefaultGSLocation = winfigSettings.value(PREFS_OTHER_USE_DEFAULT_GS_LOCATION, false).toBool();
	
	if (!useDefaultGSLocation)
	{
		// get the user defined path and check if it points at a GhostScript executable
		QString userGsLocation = winfigSettings.value(PREFS_OTHER_GS_LOCATION, "").toString();
		QFileInfo finfo(userGsLocation);

		strcpy(ghostScriptPath, finfo.path().toLatin1().data());
		strcpy(ghostScriptCommand, finfo.fileName().toLatin1().data());

		// user defined location found? Use that instead of searching registry
		if (strlen(ghostScriptPath) != 0)
		{
			return;
		}
	}

	QString ghostScriptKeyGpl = "HKEY_LOCAL_MACHINE\\SOFTWARE\\GPL Ghostscript";
	QString ghostScriptKeyAfpl = "HKEY_LOCAL_MACHINE\\SOFTWARE\\AFPL Ghostscript";

	QString key = ghostScriptKeyGpl;

	bool key64bit = true;
	QStringList groups = getGroupFromRegistry(key, QSettings::Registry64Format);

	if (groups.isEmpty())
	{
		key64bit = false;
		groups = getGroupFromRegistry(key, QSettings::Registry32Format);
	}

	if (groups.isEmpty())
	{
		key64bit = true;
		key = ghostScriptKeyAfpl;
		groups = getGroupFromRegistry(key, QSettings::Registry64Format);
	}

	if (groups.isEmpty())
	{
		key64bit = false;
		groups = getGroupFromRegistry(key, QSettings::Registry32Format);
	}

	if (groups.isEmpty())
	{
		//QLOG_INFO() << QString("key %s does not exist or has no sub-keys!").arg(key);
		strcpy(ghostScriptCommand, key64bit ? GS_EXEC64 : GS_EXEC32);
		return;
	}

	//QLOG_INFO() << QString("found registry key %1").arg(key);

	QString gsVersion = groups.last();
	QString gsVersionKey = key + "\\" + gsVersion;

	//QLOG_INFO() << QString("found registry key %1").arg(gsVersionKey);
	//QLOG_INFO() << "looking for sub-key GS_DLL";

	QSettings settings(gsVersionKey, key64bit ? QSettings::Registry64Format : QSettings::Registry32Format);
	QString pathToGsDLL = settings.value("GS_DLL").toString();

	//QLOG_INFO() << QString("value is \"") + pathToGsDLL + "\"";

	int index = pathToGsDLL.indexOf("\\bin\\");

	if (index != -1)
	{
		QString path = pathToGsDLL.left(index + 5);
		//QLOG_INFO() << QString("GhostScript executable is located in \"") + path + "\"";

		if (pathToGsDLL.contains("gsdll64"))
			strcpy(ghostScriptCommand, GS_EXEC64);
		else
			strcpy(ghostScriptCommand, GS_EXEC32);

		//QLOG_INFO() << QString("GhostScript executable path is \"") + path + "\"";

		strcpy(ghostScriptPath, path.toLatin1().data());
	}
	else
	{
		strcpy(ghostScriptCommand, key64bit ? GS_EXEC64 : GS_EXEC32);
	}
}

int WIFEXITED(int status)
{
	return status == 0;
}

int WEXITSTATUS(int status)
{
	return status;
}

#endif

#ifndef strndup

char *strndup (const char *s, size_t n)
{
  size_t len = strnlen (s, n);
  char *newstr = (char *)malloc (len + 1);

  if (newstr == NULL)
    return NULL;

  strncpy(newstr, s, len);

  newstr[len] = '\0';
  return newstr;
}

#endif 

}
