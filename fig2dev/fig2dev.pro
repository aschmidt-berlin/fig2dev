# -------------------------------------------------
# Project created by QtCreator 2009-10-21T01:02:16
# -------------------------------------------------
# QT -= gui
CONFIG += qt

# CONFIG += console
CONFIG -= app_bundle
#QMAKE_CXXFLAGS += -Wunused-parameter
TARGET = fig2dev
TEMPLATE = app

SOURCES += ../fig2dev/dev/readpng.cpp \
    ../fig2dev/dev/asc85ec.c \
    ../fig2dev/dev/genbitmaps.c \
    ../fig2dev/dev/genbox.c \
    ../fig2dev/dev/gencgm.c \
    ../fig2dev/dev/gendxf.c \
    ../fig2dev/dev/genemf.c \
    ../fig2dev/dev/genepic.c \
    ../fig2dev/dev/genge.c \
    ../fig2dev/dev/genibmgl.c \
    ../fig2dev/dev/genlatex.c \
    ../fig2dev/dev/genmap.c \
    ../fig2dev/dev/genmf.c \
    ../fig2dev/dev/genmp.c \
    ../fig2dev/dev/genpdf.c \
    ../fig2dev/dev/genpic.c \
    ../fig2dev/dev/genpictex.c \
    ../fig2dev/dev/genps.oldpatterns.c \
    ../fig2dev/dev/genpstex.c \
    ../fig2dev/dev/genpstricks.c \
    ../fig2dev/dev/genptk.c \
    ../fig2dev/dev/genshape.c \
    ../fig2dev/dev/gensvg.c \
    ../fig2dev/dev/gentextyl.c \
    ../fig2dev/dev/gentk.c \
    ../fig2dev/dev/gentpic.c \
    ../fig2dev/dev/psencode.c \
    ../fig2dev/dev/readeps.c \
    ../fig2dev/dev/readjpg.c \
    ../fig2dev/dev/readpcx.c \
    ../fig2dev/dev/readpics.c \
    ../fig2dev/dev/readppm.c \
    ../fig2dev/dev/readtif.c \
    ../fig2dev/dev/readxbm.c \
    ../fig2dev/dev/setfigfont.c \
    ../fig2dev/src/fig2dev.cpp \
    ../fig2dev/src/read_images.cpp \
    ../fig2dev/src/arrow.c \
    ../fig2dev/src/bound.c \
    ../fig2dev/src/colors.c \
    ../fig2dev/src/free.c \
    ../fig2dev/src/getopt.c \
    ../fig2dev/src/iso2tex.c \
    ../fig2dev/src/latex_line.c \
    ../fig2dev/src/localmath.c \
    ../fig2dev/src/psfonts.c \
    ../fig2dev/src/read.c \
    ../fig2dev/src/read1_3.c \
    ../fig2dev/src/strstr.c \
    ../fig2dev/src/trans_spline.c \
    ../fig2dev/src/clib.cpp \
    ../fig2dev/dev/gengbx.cpp
HEADERS += ../fig2dev/dev/genemf.h \
    ../fig2dev/dev/genlatex.h \
    ../fig2dev/dev/genpdf.h \
    ../fig2dev/dev/genps.h \
    ../fig2dev/dev/genps.oldpatterns.h \
    ../fig2dev/dev/picfonts.h \
    ../fig2dev/dev/picpsfonts.h \
    ../fig2dev/dev/psencode.h \
    ../fig2dev/dev/psfonts.h \
    ../fig2dev/dev/psimage.h \
    ../fig2dev/dev/readxbm.h \
    ../fig2dev/dev/setfigfont.h \
    ../fig2dev/dev/texfonts.h \
    ../fig2dev/dev/tpicfonts.h \
    ../fig2dev/src/alloc.h \
    ../fig2dev/src/bound.h \
    ../fig2dev/src/drivers.h \
    ../fig2dev/src/fig2dev.h \
    ../fig2dev/src/free.h \
    ../fig2dev/src/localmath.h \
    ../fig2dev/src/object.h \
    ../fig2dev/src/patchlevel.h \
    ../fig2dev/src/pi.h \
    ../fig2dev/src/read.h \
    ../fig2dev/src/trans_spline.h \
    ../fig2dev/stdafx.h \
    ../fig2dev/src/clib.h
INCLUDEPATH += ../fig2dev/src
INCLUDEPATH += ../fig2dev/Windows

#CONFIG += debug_and_release
#CONFIG(debug, debug|release) {
#    TARGET = fig2devd
#    DESTDIR = ./debug
#}
#CONFIG(release, debug|release) { Qt5Core.lib
#Qt5Gui.lib
#qtmain.lib
#ws2_32.lib
#    TARGET = fig2dev
#    DESTDIR = ./release
#}
DEFINES += BITMAPDIR \
    \"bitmaps\"
DEFINES += USE_PNG
DEFINES += I18N
DEFINES += FIG2DEV_LIBDIR \
    \"dev\"
DEFINES += NFSS
DEFINES += LATEX2E_GRAPHICS
DEFINES += DVIPS
DEFINES += WINFIG
